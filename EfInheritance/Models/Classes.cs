﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfInheritance.Models
{
    public abstract class CategoryGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }

    public abstract class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int CategoryGroupId { get; set; }
        public CategoryGroup CategoryGroup { get; set; }

        public IEnumerable<DivisionCategory> DivisionCategories { get; set; }
    }

    public abstract class DivisionCategory
    {
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int DivisionId { get; set; }
        public Division Division { get; set; }
    }

    
    public class AgeCategory : Category
    {
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
    }

    public class AgeCategoryGroup : CategoryGroup
    {


    }
    
    public class GenderCategory : Category
    {

    }

    public class GenderCategoryGroup : CategoryGroup
    {

    }

    public class AgeDivisionCategory : DivisionCategory { }
    public class GenderDivisionCategory : DivisionCategory { }

    public class Division
    {
        public int Id { get; set; }
        public IEnumerable<AgeDivisionCategory> AgeGroups { get; set; }
        public IEnumerable<GenderDivisionCategory> GenderGroups { get; set; }
    }
}
