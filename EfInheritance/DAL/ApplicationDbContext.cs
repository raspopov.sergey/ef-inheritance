﻿using EfInheritance.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfInheritance.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<AgeCategory> AgeCategories { get; set; }
        public DbSet<AgeCategoryGroup> AgeCategoryGroups { get; set; }
        public DbSet<GenderCategory> GenderCategories { get; set; }
        public DbSet<GenderCategoryGroup> GenderCategoryGroups { get; set; }

        public DbSet<Division> Divisions { get; set; }
        public DbSet<AgeDivisionCategory> AgeDivisionCategories { get; set; }
        //public DbSet<GenderDivisionCategory> GenderDivisionCategories { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ForNpgsqlUseIdentityAlwaysColumns();

            builder.Entity<DivisionCategory>().HasKey(dc => new { dc.CategoryId, dc.DivisionId });

            builder.Entity<DivisionCategory>().HasOne(dc => dc.Category)
                .WithMany(c => c.DivisionCategories).HasForeignKey(dc => dc.CategoryId);

            builder.Entity<AgeDivisionCategory>().HasOne(dc => dc.Division)
                .WithMany(d => d.AgeGroups).HasForeignKey(dc => dc.DivisionId);

            builder.Entity<GenderDivisionCategory>().HasOne(dc => dc.Division)
                .WithMany(d => d.GenderGroups).HasForeignKey(dc => dc.DivisionId);
        }
    }
}