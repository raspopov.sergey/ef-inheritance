﻿using EfInheritance.DAL;
using EfInheritance.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfInheritance.Services
{
    public class ApplicationService
    {
        private ApplicationDbContext _context;

        public ApplicationService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Category> AddCategoryAsync(Category category)
        {
            _context.Add(category);
            await _context.SaveChangesAsync();
            return category;
        }

        public async Task<IEnumerable<AgeCategory>> GetAgeCategoriesAsync()
        {
            return await _context.AgeCategories.ToListAsync();
        }

        public async Task<CategoryGroup> AddGroupAsync(CategoryGroup categoryGroup)
        {
            _context.Add(categoryGroup);
            await _context.SaveChangesAsync();
            return categoryGroup;
        }

        public async Task<Division> AddDivisionAsync(Division division)
        {
            _context.Add(division);
            await _context.SaveChangesAsync();
            return division;
        }

        public async Task<Division> UpdateDivisionCategoryAsync(Division division)
        {
            _context.Update(division);
            await _context.SaveChangesAsync();
            return division;
        }

        public async Task<Division> GetDivisionAsync(int divisionId)
        {
            return await _context.Divisions
                .FirstAsync(d => d.Id == divisionId);
        }
    }
}
