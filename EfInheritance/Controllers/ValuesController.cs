﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfInheritance.Models;
using EfInheritance.Services;
using Microsoft.AspNetCore.Mvc;

namespace EfInheritance.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ApplicationService _applicationService;

        public ValuesController(ApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        #region group

        [HttpPost("[action]")]
        public async Task<IActionResult> AgeGroup([FromBody]AgeCategoryGroup ageCategoryGroup)
        {
            try
            {
                var added = await _applicationService.AddGroupAsync(ageCategoryGroup);
                return Ok(added);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> GenderGroup([FromBody]GenderCategoryGroup genderCategoryGroup)
        {
            try
            {
                var added = await _applicationService.AddGroupAsync(genderCategoryGroup);
                return Ok(added);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion

        #region categories

        #region get

        [HttpGet("[action]")]
        public async Task<IActionResult> AgeCategory()
        {
            try
            {
                var ageCategories = await _applicationService.GetAgeCategoriesAsync();
                return Ok(ageCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion

        #region add

        [HttpPost("[action]")]
        public async Task<IActionResult> AgeCategory([FromBody]AgeCategory ageCategory)
        {
            try
            {
                var added = await _applicationService.AddCategoryAsync(ageCategory);
                return Ok(added);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
                
        [HttpPost("[action]")]
        public async Task<IActionResult> GenderCategory([FromBody] GenderCategory genderCategory)
        {
            try
            {
                await _applicationService.AddCategoryAsync(genderCategory);
                return Ok(true);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion

        #endregion

        #region division

        [HttpPost("[action]")]
        public async Task<IActionResult> Division([FromBody]Division division)
        {
            try
            {
                var added = await _applicationService.AddDivisionAsync(division);
                return Ok(added);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AgeDivisionCategory([FromBody]Division division)
        {
            try
            {
                var d = await _applicationService.GetDivisionAsync(division.Id);
                d.AgeGroups = division.AgeGroups;
                var udpated = await _applicationService.UpdateDivisionCategoryAsync(d);
                return Ok(udpated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> GenderDivisionCategory([FromBody]Division division)
        {
            try
            {
                var udpated = await _applicationService.UpdateDivisionCategoryAsync(division);
                return Ok(udpated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion
    }
}
